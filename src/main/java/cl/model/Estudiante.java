package cl.model;

public class Estudiante 
{
    private String nombre;
    private String seccion;
    
    public Estudiante(String nombre, String seccion)
    {
        this.nombre = nombre;
        this.seccion = seccion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getSeccion() {
        return this.seccion;
    }
    
    public String getDatos()
    { 
        String resul = "Estudiante: " + getNombre() + "\nSección: " + getSeccion();
        return resul;
    }   
           
    
}
