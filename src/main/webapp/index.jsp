<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="frmDatosEstudiante" action="ControllerEstudiante" method="POST">
            <h1>Datos del Estudiante</h1>
            <br>
            <label for="txtNombre" class="Label">Ingresa tu nombre:</label>
            <input type="text" name="txtNombre" id="txtNombre">
            <br>
            <label for="txtSeccion" class="Label">Ingresa tu Sección:</label>
            <input type="text" name="txtSeccion" id="txtSeccion">
            <br>
            <br>
            <button type="submit" name="btnEnviar" id="btnEnviar" value="Enviar datos">Enviar datos</button>
        </form>
    </body>
</html>
