<%@page import="cl.model.Estudiante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Estudiante e1 = (Estudiante)request.getAttribute("estudiante");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="frmResultado" action="EdadController" method="POST">
            <h1>Datos del Estudiante</h1>
            <br>
            Nombre: <%= e1.getNombre() %>
            <br>
            Sección: <%= e1.getSeccion() %>
            <br>
            <br>
            <button type="submit" name="btnVolver" id="btnVolver" value="Volver">Volver</button>
        </form>
    </body>
</html>
